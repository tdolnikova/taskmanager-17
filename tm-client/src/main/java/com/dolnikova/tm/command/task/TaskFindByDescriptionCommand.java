package com.dolnikova.tm.command.task;

import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.AdditionalMessage;
import com.dolnikova.tm.constant.Command;
import com.dolnikova.tm.endpoint.TaskDTO;
import com.dolnikova.tm.endpoint.TaskEndpoint;
import com.dolnikova.tm.endpoint.UserDTO;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public final class TaskFindByDescriptionCommand extends AbstractCommand {

    @Autowired
    private TaskEndpoint taskEndpoint;

    @NotNull
    @Override
    public String command() {
        return Command.FIND_TASK_BY_DESCRIPTION;
    }

    @NotNull
    @Override
    public String description() {
        return Command.FIND_TASK_BY_DESCRIPTION_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        if (!isSecure()) return;
        @NotNull String userInput = "";
        System.out.println(AdditionalMessage.INSERT_TEXT);
        while (userInput.isEmpty()) {
            userInput = serviceLocator.getScanner().nextLine();
        }
        @Nullable final List<TaskDTO> taskList = taskEndpoint.findAllByDescriptionTask(serviceLocator.getSessionDTO(), userInput);
        for (final TaskDTO task : taskList) {
            System.out.println(task.getName());
        }
    }

    @Override
    public boolean isSecure() {
        UserDTO currentUser = serviceLocator.getUserDTO();
        if (currentUser == null) return false;
        return currentUser.getId() != null;
    }
}
