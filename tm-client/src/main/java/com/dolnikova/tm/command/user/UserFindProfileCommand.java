package com.dolnikova.tm.command.user;

import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.Command;
import com.dolnikova.tm.endpoint.UserDTO;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class UserFindProfileCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return Command.USER_FIND_PROFILE;
    }

    @NotNull
    @Override
    public String description() {
        return Command.USER_FIND_PROFILE_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        if (!isSecure()) return;
        @Nullable UserDTO user = serviceLocator.getUserDTO();
        if (user == null) {
            System.out.println("Нет авторизации.");
            return;
        }
        System.out.println("id: " + user.getId());
        System.out.println("login: " + user.getLogin());
        System.out.println("password: " + user.getPasswordHash());
        System.out.println("role: " + user.getRole());
    }

    @Override
    public boolean isSecure() {
        UserDTO currentUser = serviceLocator.getUserDTO();
        if (currentUser == null) return false;
        return currentUser.getId() != null;
    }

}
