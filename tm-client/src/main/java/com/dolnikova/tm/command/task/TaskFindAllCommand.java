package com.dolnikova.tm.command.task;

import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.AdditionalMessage;
import com.dolnikova.tm.constant.Command;
import com.dolnikova.tm.endpoint.TaskDTO;
import com.dolnikova.tm.endpoint.TaskEndpoint;
import com.dolnikova.tm.endpoint.UserDTO;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public final class TaskFindAllCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return Command.FIND_ALL_TASKS;
    }

    @NotNull
    @Override
    public String description() {
        return Command.FIND_ALL_TASKS_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        if(!isSecure()) return;
        @Nullable final List<TaskDTO> allTasks = serviceLocator.getTaskEndpoint().findAllTask(serviceLocator.getSessionDTO());
        if (allTasks == null || allTasks.isEmpty()) System.out.println(AdditionalMessage.NO_TASKS);
        else {
            for (final TaskDTO task : allTasks) {
                System.out.println(task.getName());
            }
        }
    }

    @Override
    public boolean isSecure() {
        UserDTO currentUser = serviceLocator.getUserDTO();
        if (currentUser == null) return false;
        return currentUser.getId() != null;
    }

}
