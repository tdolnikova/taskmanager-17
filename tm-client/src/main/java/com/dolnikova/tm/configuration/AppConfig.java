package com.dolnikova.tm.configuration;

import com.dolnikova.tm.constant.General;
import com.dolnikova.tm.endpoint.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

@Configuration
@ComponentScan("com.dolnikova.tm")
public class AppConfig {

    @Bean
    SessionDTO setSessionDTOO() {
        SessionDTO sessionDTO = new SessionDTO();
        return sessionDTO;
    }

    @Bean
    UserDTO setUserDTO() {
        UserDTO userDTO = new UserDTO();
        return userDTO;
    }

    @Bean
    Scanner getScanner() {
        return new Scanner(System.in);
    }

    @Bean
    TaskEndpoint getTaskEndpoint() throws MalformedURLException {
        URL taskUrl = new URL(General.ADDRESS + "TaskEndpoint?WSDL");
        QName taskQname = new QName("http://endpoint.tm.dolnikova.com/", "TaskEndpointService");
        Service taskService = Service.create(taskUrl, taskQname);
        return taskService.getPort(TaskEndpoint.class);
    }

    @Bean
    ProjectEndpoint getProjectEndpoint() throws MalformedURLException {
        URL projectUrl = new URL(General.ADDRESS + "ProjectEndpoint?WSDL");
        QName projectQname = new QName("http://endpoint.tm.dolnikova.com/", "ProjectEndpointService");
        Service projectService = Service.create(projectUrl, projectQname);
        return projectService.getPort(ProjectEndpoint.class);
    }

    @Bean
    UserEndpoint getUserEndpoint() throws MalformedURLException {
        URL userUrl = new URL(General.ADDRESS + "UserEndpoint?WSDL");
        QName userQname = new QName("http://endpoint.tm.dolnikova.com/", "UserEndpointService");
        Service userService = Service.create(userUrl, userQname);
        return userService.getPort(UserEndpoint.class);
    }

    @Bean
    SessionEndpoint getSessionEndpoint() throws MalformedURLException {
        URL sessionUrl = new URL(General.ADDRESS + "SessionEndpoint?WSDL");
        QName sessionQname = new QName("http://endpoint.tm.dolnikova.com/", "SessionEndpointService");
        Service sessionService = Service.create(sessionUrl, sessionQname);
        return sessionService.getPort(SessionEndpoint.class);
    }

}
