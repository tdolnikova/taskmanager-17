package com.dolnikova.tm.bootstrap;

import com.dolnikova.tm.api.EndpointLocator;
import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.endpoint.*;
import com.dolnikova.tm.exception.CommandCorruptException;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.Exception;
import java.util.*;

@Getter
@Setter
@Component
public final class Bootstrap implements EndpointLocator {

    @Nullable
    @Autowired
    private UserEndpoint userEndpoint;

    @Nullable
    @Autowired
    private ProjectEndpoint projectEndpoint;

    @Nullable
    @Autowired
    private TaskEndpoint taskEndpoint;

    @Nullable
    @Autowired
    private SessionEndpoint sessionEndpoint;

    @NotNull
    @Autowired
    private Scanner scanner;

    @NotNull
    @Autowired
    private SessionDTO sessionDTO;

    @NotNull
    @Autowired
    private UserDTO userDTO;

    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @NotNull
    private final Set<Class<? extends AbstractCommand>> classes = new Reflections("com.dolnikova.tm").getSubTypesOf(AbstractCommand.class);

    public void init(@NotNull Class[] CLASSES) {
        try {
            for (@NotNull final Class clazz : CLASSES) {
                if (!AbstractCommand.class.isAssignableFrom(clazz)) continue;
                registryCommand((AbstractCommand) clazz.newInstance());
            }
            start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void registryCommand(@NotNull final AbstractCommand command) throws CommandCorruptException {
        @Nullable final String cliCommand = command.command();
        @Nullable final String cliDescription = command.description();
        if (cliCommand.isEmpty())
            throw new CommandCorruptException();
        if (cliDescription.isEmpty())
            throw new CommandCorruptException();
        command.setServiceLocator(this);
        commands.put(cliCommand, command);
    }

    private void start() {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        if (getSessionDTO().getId() != null) System.out.println("Current session: " + getSessionDTO().getId());
        @NotNull String command = "";
        while (!"exit".equals(command)) {
            try {
                command = scanner.nextLine();
                if (command.isEmpty()) continue;
                if (commands.get(command) == null) throw new CommandCorruptException();
                execute(command);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void execute(final String command) throws Exception {
        if (command == null || command.isEmpty()) return;
        @Nullable final AbstractCommand abstractCommand = commands.get(command);
        if (abstractCommand == null) return;
        abstractCommand.execute();
    }

    @NotNull
    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }

}