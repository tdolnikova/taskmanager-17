package com.dolnikova.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

public final class DateUtil {

    @NotNull private static final String DATE_FORMAT = "DD.MM.YYYY";
    @Nullable private static final SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT, Locale.ENGLISH);

    @Nullable
    public static XMLGregorianCalendar stringToXMLGregorianCalendar(@Nullable final String dateString){
        @NotNull final GregorianCalendar gCalendar = new GregorianCalendar();
        @Nullable XMLGregorianCalendar xmlCalendar = null;
        try {
            Date date = dateFormat.parse(dateString);
            gCalendar.setTime(date);
            xmlCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(gCalendar);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return xmlCalendar;
    }

}
