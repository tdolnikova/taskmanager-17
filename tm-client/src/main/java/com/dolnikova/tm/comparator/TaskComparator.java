package com.dolnikova.tm.comparator;

import com.dolnikova.tm.endpoint.TaskDTO;
import org.jetbrains.annotations.Nullable;

import java.util.Comparator;

public class TaskComparator {

    @Nullable
    public static Comparator<TaskDTO> getStatusComparator() {
        return new Comparator<TaskDTO>() {
            @Override
            public int compare(TaskDTO o1, TaskDTO o2) {
                return o1.getStatus().compareTo(o2.getStatus());
            }
        };
    }

    @Nullable
    public static Comparator<TaskDTO> getCreationDateComparator() {
        return new Comparator<TaskDTO>() {
            @Override
            public int compare(TaskDTO o1, TaskDTO o2) {
                return o1.getCreationDate().toGregorianCalendar().compareTo
                        (o2.getCreationDate().toGregorianCalendar());
            }
        };
    }

    @Nullable
    public static Comparator<TaskDTO> getStartDateComparator() {
        return new Comparator<TaskDTO>() {
            @Override
            public int compare(TaskDTO o1, TaskDTO o2) {
                return o1.getDateBegin().toGregorianCalendar().compareTo
                        (o2.getDateBegin().toGregorianCalendar());
            }
        };
    }

    @Nullable
    public static Comparator<TaskDTO> getEndDateComparator() {
        return new Comparator<TaskDTO>() {
            @Override
            public int compare(TaskDTO o1, TaskDTO o2) {
                return o1.getDateEnd().toGregorianCalendar().compareTo
                        (o2.getDateEnd().toGregorianCalendar());
            }
        };
    }

}
