package com.dolnikova.tm;

import com.dolnikova.tm.api.service.IUserService;
import com.dolnikova.tm.entity.User;
import com.dolnikova.tm.enumerated.DataType;
import org.apache.deltaspike.testcontrol.api.junit.CdiTestRunner;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;

@RunWith(CdiTestRunner.class)
public class UserTest {

    @Inject
    IUserService userService;

    @Before
    public void startTest() {
        System.out.println("[START]");
    }

    @After
    public void finishTest() {
        System.out.println("[FINISH]");
    }

    @Test
    public void persistAndFind() {
        User user = new User();
        user.setLogin("123");
        user.setPasswordHash("123");
        userService.persist(user);
        User savedUser = userService.findOneByLogin("123");
        Assert.assertNotNull(savedUser);
    }

    @Test
    public void merge() {
        User user = new User();
        user.setLogin("123");
        user.setPasswordHash("123");
        userService.persist(user);
        User savedUser = userService.findOneByLogin("123");
        Assert.assertNotNull(savedUser);

        userService.merge(
                "merge-test",
                savedUser,
                DataType.LOGIN
        );
        savedUser = userService.findOneByLogin("merge-test");
        Assert.assertNotNull(savedUser);
    }

}

