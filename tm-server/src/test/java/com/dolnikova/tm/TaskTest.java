package com.dolnikova.tm;

import com.dolnikova.tm.entity.Project;
import com.dolnikova.tm.entity.Task;
import com.dolnikova.tm.entity.User;
import com.dolnikova.tm.service.ProjectService;
import com.dolnikova.tm.service.TaskService;
import com.dolnikova.tm.service.UserService;
import org.apache.deltaspike.testcontrol.api.junit.CdiTestRunner;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;

@RunWith(CdiTestRunner.class)
public class TaskTest {

    @Inject
    ProjectService projectService;

    @Inject
    TaskService taskService;

    @Inject
    UserService userService;

    private User user;
    private Project project;

    @Before
    public void startTest() {
        System.out.println("[START]");
        user = new User();
        user.setLogin("123");
        user.setPasswordHash("123");
        userService.persist(user);
        user = userService.findOneByLogin("123");
        Assert.assertNotNull(user);

        project = new Project();
        project.setName("123");
        project.setUser(user);
        projectService.persist(project);
        project = projectService.findOneByName("123");
        Assert.assertNotNull(project);
    }

    @After
    public void finishTest() {
        System.out.println("[FINISH]");
    }

    @Test
    public void persistAndDelete() {
        Task task = new Task();
        task.setName("123");
        task.setDescription("123");
        task.setUser(user);
        task.setProject(project);
        taskService.persist(task);
        Task savedTaskName = taskService.findOneByName("123", user.getId());
        Assert.assertNotNull(savedTaskName);
        Task savedTaskId = taskService.findOneById(user.getId(), savedTaskName.getId());
        Assert.assertNotNull(savedTaskId);
    }

}
