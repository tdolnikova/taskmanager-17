package com.dolnikova.tm;

import com.dolnikova.tm.entity.Session;
import com.dolnikova.tm.entity.User;
import com.dolnikova.tm.service.SessionService;
import com.dolnikova.tm.service.UserService;
import org.apache.deltaspike.testcontrol.api.junit.CdiTestRunner;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;
import java.util.Collection;

@RunWith(CdiTestRunner.class)
public class SessionTest {

    @Inject
    SessionService sessionService;

    @Inject
    UserService userService;

    private User user;

    @Before
    public void startTest() {
        System.out.println("[START]");
        user = new User();
        user.setLogin("123");
        user.setPasswordHash("123");
        userService.persist(user);
        user = userService.findOneByLogin("123");
        Assert.assertNotNull(user);
    }

    @After
    public void finishTest() {
        System.out.println("[FINISH]");
    }

    @Test
    public void persistAndFind() {
        Session session = new Session();
        session.setUser(user);
        sessionService.persist(session);
        Collection<Session> collection = sessionService.findAllByUserId(user.getId());
        Assert.assertNotNull(collection);
    }

}
