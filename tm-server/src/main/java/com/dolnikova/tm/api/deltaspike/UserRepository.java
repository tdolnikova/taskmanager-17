package com.dolnikova.tm.api.deltaspike;

import com.dolnikova.tm.entity.User;
import org.apache.deltaspike.data.api.FullEntityRepository;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.Repository;
import org.jetbrains.annotations.Nullable;

import javax.persistence.QueryHint;
import java.util.Collection;

@Repository(forEntity = User.class)
public interface UserRepository extends FullEntityRepository<User, String> {

    @Query(value = "select u from User u where u.login = ?1", hints = {@QueryHint(name = "org.hibernate.cacheable", value = "true")})
    User findUserByLogin(@Nullable String login);

    @Query(value = "select u from User u where u.id = ?1", hints = {@QueryHint(name = "org.hibernate.cacheable", value = "true")})
    User findUserById(@Nullable String id);

    @Query(value = "select u from User u where u.login = ?1", hints = {@QueryHint(name = "org.hibernate.cacheable", value = "true")})
    Collection<User> findAllByLogin(@Nullable String login);

}
