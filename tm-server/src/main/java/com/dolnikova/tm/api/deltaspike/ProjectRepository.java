package com.dolnikova.tm.api.deltaspike;

import com.dolnikova.tm.entity.Project;
import org.apache.deltaspike.data.api.FullEntityRepository;
import org.apache.deltaspike.data.api.Modifying;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.Repository;
import org.apache.deltaspike.jpa.api.transaction.Transactional;
import org.jetbrains.annotations.Nullable;

import javax.persistence.QueryHint;
import java.util.Collection;

@Repository(forEntity = Project.class)
public interface ProjectRepository extends FullEntityRepository<Project, String> {

    @Query(value = "select p from Project p where p.id = ?1 and p.user.id = ?2", hints = {@QueryHint(name = "org.hibernate.cacheable", value = "true")})
    Project findProjectByIdAndUserId(@Nullable String id, @Nullable String userId);

    @Query(value = "select p from Project p where p.name = ?1", hints = {@QueryHint(name = "org.hibernate.cacheable", value = "true")})
    Project findProjectByName(@Nullable String name);

    @Query(value = "select p from Project p where p.user.id = ?1", hints = {@QueryHint(name = "org.hibernate.cacheable", value = "true")})
    Collection<Project> findAllByUserId(@Nullable String userId);

    @Query(value = "select p from Project p where p.user.id = ?1 and p.name like ?2", hints = {@QueryHint(name = "org.hibernate.cacheable", value = "true")})
    Collection<Project> findAllByUserIdAndNameContaining(@Nullable String userId, @Nullable String text);

    @Query(value = "select p from Project p where p.user.id = ?1 and p.description like ?2", hints = {@QueryHint(name = "org.hibernate.cacheable", value = "true")})
    Collection<Project> findAllByUserIdAndDescriptionContaining(@Nullable String userId, @Nullable String text);

    @Modifying
    @Transactional
    @Query("delete from Project p where p.user.id = ?1")
    void deleteAllByUserId(@Nullable String userId);

}
