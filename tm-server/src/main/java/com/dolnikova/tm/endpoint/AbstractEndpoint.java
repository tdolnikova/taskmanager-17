package com.dolnikova.tm.endpoint;

import com.dolnikova.tm.api.service.IProjectService;
import com.dolnikova.tm.api.service.ISessionService;
import com.dolnikova.tm.api.service.ITaskService;
import com.dolnikova.tm.api.service.IUserService;
import com.dolnikova.tm.entity.Session;
import com.dolnikova.tm.entity.User;
import com.dolnikova.tm.exception.SessionExpiredException;
import com.dolnikova.tm.exception.SessionNotFoundException;
import com.dolnikova.tm.util.SignatureUtil;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.inject.Inject;

@NoArgsConstructor
abstract class AbstractEndpoint {

    @Inject
    IUserService userService;
    @Inject
    IProjectService projectService;
    @Inject
    ITaskService taskService;
    @Inject
    ISessionService sessionService;


    void validate(@Nullable final Session session) {
        try {
            if (session == null) throw new SessionNotFoundException();

            @NotNull final Long currentTime = System.currentTimeMillis();
            @NotNull final Long sessionTime = session.getTimestamp();
            if ((currentTime - sessionTime > 100000)) throw new SessionExpiredException();

            @Nullable final String sessionSignature = session.getSignature();
            @Nullable final String calculatedSignature = SignatureUtil.sign(session, "JAVA", 3);
            if (sessionSignature == null
                    || sessionSignature.isEmpty()
                    || !sessionSignature.equals(calculatedSignature)) {
                throw new SessionNotFoundException();
            }
            @Nullable final User user = session.getUser();
            if (user == null) return;
            @Nullable final Session foundSession = sessionService.findOneById(user.getId(), session.getId());
            if (foundSession == null) throw new SessionNotFoundException();
        } catch (SessionNotFoundException | SessionExpiredException e) {
            e.getMessage();
        }
    }

}
