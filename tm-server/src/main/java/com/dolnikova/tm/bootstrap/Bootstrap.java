package com.dolnikova.tm.bootstrap;

import com.dolnikova.tm.endpoint.ProjectEndpoint;
import com.dolnikova.tm.endpoint.SessionEndpoint;
import com.dolnikova.tm.endpoint.TaskEndpoint;
import com.dolnikova.tm.endpoint.UserEndpoint;
import lombok.Getter;
import lombok.Setter;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.xml.ws.Endpoint;
import java.util.Scanner;

@Getter
@Setter
@Singleton
public final class Bootstrap {

    @Inject
    private UserEndpoint userEndpoint;
    @Inject
    private TaskEndpoint taskEndpoint;
    @Inject
    private ProjectEndpoint projectEndpoint;
    @Inject
    private SessionEndpoint sessionEndpoint;

    public void init() {
        Endpoint.publish("http://localhost:8080/TaskEndpoint?WSDL", taskEndpoint);
        Endpoint.publish("http://localhost:8080/ProjectEndpoint?WSDL", projectEndpoint);
        Endpoint.publish("http://localhost:8080/UserEndpoint?WSDL", userEndpoint);
        Endpoint.publish("http://localhost:8080/SessionEndpoint?WSDL", sessionEndpoint);

        Scanner scanner = new Scanner(System.in);
        String input = "";
        while (!"exit".equals(input)) {
            try {
                input = scanner.nextLine();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        System.exit(0);
    }

}
